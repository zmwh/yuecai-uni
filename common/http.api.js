// 如果没有通过拦截器配置域名的话，可以在这里写上完整的URL(加上域名部分)
let imgCode = '/auth/code';
let userLogin = '/auth/api/login';
let oneCategory = '/category/ycCategory/category';
let towCategory = '/category/ycCategory';
let goodsListUrl = '/good/ycGoods';
let goodsInfoUrl = '/good/ycGoods/id';
let goodsSkuInfoUrl = '/good/ycGoods/details';
let addCarUrl = '/api/ycCart';
let delCarUrl = '/api/ycCart/delete';
let skuCarUrl = '/api/ycCart/sku';
let carListUrl = '/api/ycCart';
let writeOrderUrl = '/api/order/write-order';
let addSnapshootUrl = '/api/ycGoodsEnshrineSnapshoot';
let checkoutSnapshootUrl = 'api/ycGoodsEnshrineSnapshoot/checkout'
let receiverUrl = '/api/ycReceiver';
let editReceiverUrl = '/api/ycReceiver/edit';
let addReceiverUrl = '/api/ycReceiver';
let searchReceiverUrl = '/api/ycReceiver';
let submitOrderUrl = '/api/order/submit-order';
let orderListUrl = '/api/order';
let cancelOrderUrl = '/api/order/cancel';
let updateUrl = '/good/ycGoods/update';
let findOrderUrl = '/api/order/find-order';
let sendCodeUrl = '/auth/api/send-code';
let checkCodeUrl = '/auth/api/check-code';
let loginCodeUrl = '/auth/api/login-code';
let qiNiuUrl = '/api/qiNiuContent';
let realNameUrl = '/api/yc-real-name';
let logoutUrl = '/auth/logout';
let bannerUrl = '/banner/ycBannerConf'
let authInfoUrl = '/auth/info'
let fixedInfoUrl = '/yc-fixed'
let sjOrderUrl = '/api/order/sj'
let couponListUrl = '/api/ycCoupon/list'
let userCouponListUrl = '/ycUserCoupon'
let giveCouponListUrl = '/api/ycCoupon/give'
let showPriceUrl = '/api/order/showPrice'
let xcxLoginUrl = '/auth/api/wx/user/login'
let bindingUrl = '/auth/api/wx/user/binding'
let noticeUrl = '/ycNotice'
let ywOrderUrl = '/api/order/yw'

// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
// https://uviewui.com/js/http.html#%E4%BD%95%E8%B0%93%E8%AF%B7%E6%B1%82%E6%8B%A6%E6%88%AA%EF%BC%9F
const install = (Vue, vm) => {
	// 公告
	let notice = (params = {}) => vm.$u.get(noticeUrl, params);
	// 绑定接口
	let binding = (params = {}) => vm.$u.post(bindingUrl, params);
	// 小程序登录
	let xcxLogin = (params = {}) => vm.$u.get(xcxLoginUrl, params);
	// 登录/注册发生验证码
	let sendCode = (params = {}) => vm.$u.get(sendCodeUrl, params);
	// 登录验证
	let loginCode = (params = {}) => vm.$u.post(loginCodeUrl, params);
	// 验证
	let checkCode = (params = {}) => vm.$u.post(checkCodeUrl, params);
	// 升级
	let update = (params = {}) => vm.$u.post(updateUrl, params);
	// 获取图片验证码
	let getImgCode = (params = {}) => vm.$u.get(imgCode, params);
	// app 登录
	let postUserLogin = (params = {}) => vm.$u.post(userLogin, params);
	// 一级分类
	let getOneCategory = (params = {}) => vm.$u.get(oneCategory, params);
	// 二级分类
	let getTwoCategory = (params = {}) => vm.$u.get(towCategory, params);
	// 查看商品
	let getGoodsList = (params = {}) => vm.$u.get(goodsListUrl, params);
	// 查看商品
	let getGoodsInfo = (params = {}) => vm.$u.get(goodsInfoUrl, params);
	// 查看商品
	let getGoodsSkuInfoUrl = (params = {}) => vm.$u.get(goodsSkuInfoUrl, params);
	// 新增购物车
	let postAddCar = (params = {}) => vm.$u.post(addCarUrl, params);
	// 删除购物车
	let delAddCar = (params = {}) => vm.$u.post(delCarUrl, params);
	// 添加收藏
	let addSnapshoot = (params = {}) => vm.$u.post(addSnapshootUrl, params);
	// 返回收藏
	let checkoutSnapshoot = (params = {}) => vm.$u.post(checkoutSnapshootUrl, params);
	// 单个sku购物车信息
	let getSkuCar = (params = {}) => vm.$u.get(skuCarUrl, params);
	// 购物车信息
	let getCarList = (params = {}) => vm.$u.get(carListUrl, params);
	// 填写订单信息
	let getWriteOrderInfo = (params = {}) => vm.$u.post(writeOrderUrl, params);
	// 提交订单信息
	let submitOrder = (params = {}) => vm.$u.post(submitOrderUrl, params);
	// 查询收获地址
	let searchReceiver = (params = {}) => vm.$u.get(searchReceiverUrl, params);
	// 修改收获地址
	let editReceiver = (params = {}) => vm.$u.post(editReceiverUrl, params);
	// create收获地址
	let createReceiver = (params = {}) => vm.$u.post(addReceiverUrl, params);
	// 删除收获地址
	let delReceiver = (params = {}) => vm.$u.delete(receiverUrl, params);
	// 首页分类
	let getHomeMenu = (params = {}) => vm.$u.get(oneCategory, params);
	// 查询订单列表
	let getOrderList = (params = {}) => vm.$u.get(orderListUrl, params);
	// 查询订单列表
	let findOrderList = (params = {}) => vm.$u.get(findOrderUrl, params);
	// 七牛云上传图片
	let upQiNiu = (params = {}) => vm.$u.post(qiNiuUrl, params);
	// 上传实名信息
	let realName = (params = {}) => vm.$u.post(realNameUrl, params);
	// 退出登录
	let logout = (params = {}) => vm.$u.delete(logoutUrl, params);
	//bannerUrl
	let getBanner = (params = {}) => vm.$u.get(bannerUrl, params);
	//取消订单
	let cancelOrder = (params = {}) => vm.$u.put(cancelOrderUrl, params);
	//获取用户信息
	let getAuthInfo = (params = {}) => vm.$u.get(authInfoUrl, params);
	//获取配置信息
	let getFixedInfo = (params = {}) => vm.$u.get(fixedInfoUrl, params);
	//司机订单
	let getSjOrder = (params = {}) => vm.$u.get(sjOrderUrl, params);
	//全部优惠券
	let getCouponList = (params = {}) => vm.$u.get(couponListUrl, params);
	//用户优惠券
	let getUserCouponList = (params = {}) => vm.$u.get(userCouponListUrl, params);
	//领取用户优惠券
	let giveUserCouponList = (params = {}) => vm.$u.get(giveCouponListUrl, params);
	//计算优惠价格
	let showPrice = (params = {}) => vm.$u.post(showPriceUrl, params);
	//业务员订单
	let getYwOrder = (params = {}) => vm.$u.get(ywOrderUrl, params);
	
	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		notice,
		binding,
		xcxLogin,
		update,
		getImgCode,
		postUserLogin,
		getOneCategory,
		getTwoCategory,
		getGoodsList,
		getGoodsInfo,
		getGoodsSkuInfoUrl,
		postAddCar,
		getSkuCar,
		getCarList,
		getWriteOrderInfo,
		addSnapshoot,
		delAddCar,
		searchReceiver,
		submitOrder,
		editReceiver,
		createReceiver,
		getHomeMenu,
		checkoutSnapshoot,
		getOrderList,
		findOrderList,
		sendCode,
		checkCode,
		loginCode,
		upQiNiu,
		realName,
		logout,
		getBanner,
		cancelOrder,
		getAuthInfo,
		getFixedInfo,
		getSjOrder,
		getCouponList,
		getUserCouponList,
		giveUserCouponList,
		delReceiver,
		showPrice,
		getYwOrder,
	};
}

export default {
	install
}
